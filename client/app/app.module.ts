console.log('In app.module.ts');
import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { HttpModule }     from '@angular/http'
import { FormsModule }    from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import { AppComponent }   from './app.component';
import { HomeComponent } from './components/home.component';
import { ListComponent } from "./components/list.component";

const appRoutes: Routes =[
    { path: '', component: HomeComponent},
    /*{ path: 'list', component: ListComponent}*/
];

@NgModule({
  imports:      [ BrowserModule, HttpModule, FormsModule, RouterModule.forRoot(appRoutes) ],
    providers :[
        { provide: APP_BASE_HREF9, useValue: '/' }
    ],
  declarations: [ AppComponent, HomeComponent/*, ListComponent */],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }
